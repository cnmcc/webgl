"use strict";

var gl;
var points = [];
var NumTimesToSubdivide = 4;

window.onload = function init() {
    var canvas = document.getElementById("gl-canvas");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn't available");
    }

    // initial tetrahedron
    var vertices = [
        vec3(0, 0, -1),
        vec3(0, 0.9428, 0.3333),
        vec3(-0.8165, -0.4714, 0.3333),
        vec3(0.8165, -0.4714, 0.3333)
    ];

    divideTriangle(vertices[0], vertices[1], vertices[2], NumTimesToSubdivide); // left side
    divideTriangle(vertices[0], vertices[1], vertices[3], NumTimesToSubdivide); // right side
    divideTriangle(vertices[0], vertices[2], vertices[3], NumTimesToSubdivide); // bottom side
    divideTriangle(vertices[1], vertices[2], vertices[3], NumTimesToSubdivide); // front side

    // Configure WebGL
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.9, 0.9, 1.0, 1.0); // use a blue pastel background
    gl.enable(gl.DEPTH_TEST);

    // Load shaders and initialize attribute buffers
    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    // Load the data into the GPU
    var bufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW);

    // Associate shader variables with variables in JS file
    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    render();
};

function render() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.drawArrays(gl.TRIANGLES, 0, points.length);
}

function divideTriangle(a, b, c, count) {
    // check for end of recursion
    if (count === 0) {
        points.push(a, b, c);
    } else {
        // bisect the sides
        var ab = mix(a, b, 0.5);
        var ac = mix(a, c, 0.5);
        var bc = mix(b, c, 0.5);

        // three new triangles
        count--;
        divideTriangle(a, ab, ac, count);
        divideTriangle(c, ac, bc, count);
        divideTriangle(b, bc, ab, count);
    }
}
