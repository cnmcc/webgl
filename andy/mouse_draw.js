"use strict";

var gl;
var maxNumLines = 10000;
var maxNumVertices = 2 * maxNumLines;
var index = 0, isDrawing = false, prevVertex;
var ColorCtrl;

window.onload = function init() {
    angular.bootstrap(document, ["mouseDrawApp"]);

    var canvas = document.getElementById("gl-canvas");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn't available");
    }

    // Configure WebGL
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.9, 0.9, 1.0, 1.0); // use a blue pastel background

    // Load shaders and initialize attribute buffers
    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    // Initialize the vertex attribute buffer
    var vBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, 8 * maxNumVertices, gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    // Initialize the color attribute buffer
    var cBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, cBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, 16 * maxNumVertices, gl.STATIC_DRAW);

    var vColor = gl.getAttribLocation(program, "vColor");
    gl.vertexAttribPointer(vColor, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vColor);

    // Convert mouse event coordinates to vertex coordinates
    var mouseToVertex = function(event) {
        var x = event.clientX - event.target.offsetLeft,
            y = event.clientY - event.target.offsetTop;

        return vec2((2 * x / canvas.width) - 1, 1 - (2 * y / canvas.height));
    };

    // Event handlers
    canvas.addEventListener("mousedown", function(event) {
        prevVertex = mouseToVertex(event);
        isDrawing = true;
    });

    canvas.addEventListener("mouseup", function() {
        isDrawing = false;
    });

    canvas.addEventListener("mousemove", function(event) {
        if (isDrawing && index < maxNumLines) {
            var curVertex = mouseToVertex(event);

            // Buffer a line with two vertices
            gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
            gl.bufferSubData(gl.ARRAY_BUFFER, 16 * index, flatten(prevVertex));
            gl.bufferSubData(gl.ARRAY_BUFFER, 16 * index + 8, flatten(curVertex));

            // Buffer the vertex colors
            gl.bindBuffer(gl.ARRAY_BUFFER, cBuffer);
            gl.bufferSubData(gl.ARRAY_BUFFER, 32 * index, flatten(ColorCtrl.getCurrentVColor()));
            gl.bufferSubData(gl.ARRAY_BUFFER, 32 * index + 16, flatten(ColorCtrl.getNextVColor()));

            index++;
            prevVertex = curVertex;
        }
    });

    render();
};

function render() {
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.drawArrays(gl.LINES, 0, index * 2);
    window.requestAnimFrame(render);
}

// AngularJS UI logic
angular.module("mouseDrawApp", [])
    .controller("ColorCtrl", [function () {
        var self = this;
        self.colors = [
            {name: "rainbow"                                   },
            {name: "red",       color: vec4(1.0, 0.0, 0.0, 1.0)},
            {name: "yellow",    color: vec4(0.9, 0.9, 0.0, 1.0)},
            {name: "green",     color: vec4(0.0, 0.8, 0.0, 1.0)},
            {name: "turquoise", color: vec4(0.0, 0.8, 0.8, 1.0)},
            {name: "blue",      color: vec4(0.0, 0.0, 1.0, 1.0)},
            {name: "violet",    color: vec4(1.0, 0.0, 1.0, 1.0)}
        ];
        self.rainbowIndex = 1;
        self.selectedColor = self.colors[self.rainbowIndex];
        self.getCurrentVColor = function () {
            if (self.selectedColor == self.colors[0]) {
                return self.colors[self.rainbowIndex].color;
            } else {
                return self.selectedColor.color;
            }
        };
        self.getNextVColor = function () {
            if (self.selectedColor == self.colors[0] &&
                ++self.rainbowIndex == self.colors.length) {
                self.rainbowIndex = 1;
            }
            return self.getCurrentVColor();
        };
        self.eraseCanvas = function () {
            index = 0;
        };
        ColorCtrl = self;
    }]);
