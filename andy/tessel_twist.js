"use strict";

var gl;
var points, NumTimesToSubdivide, DegreesToRotate, theta;

window.onload = function init() {
    angular.bootstrap(document, ["tesselApp"]);

    var canvas = document.getElementById("gl-canvas");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn't available");
    }

    // Configure WebGL
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.9, 0.9, 1.0, 1.0); // use a blue pastel background

    // Load shaders and initialize attribute buffers
    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    // Load the data into the GPU
    var bufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
    tesselateAndLoadTriangle();

    // Associate shader variables with variables in JS file
    var color = gl.getUniformLocation(program, "color");
    gl.uniform4f(color, 0.4, 0.2, 0.6, 1.0); // rebeccapurple (opaque)
    theta = gl.getUniformLocation(program, "theta");
    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    render();
};

function render() {
    gl.uniform1f(theta, radians(DegreesToRotate));
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.drawArrays(gl.TRIANGLES, 0, points.length);
}

function tesselateAndLoadTriangle() {
    var vertices = [
        vec2(-0.86603, -0.5),
        vec2(0, 1),
        vec2(0.86603, -0.5)
    ];

    points = [];
    divideTriangle(vertices[0], vertices[1], vertices[2], NumTimesToSubdivide);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW);
}

function divideTriangle(a, b, c, count) {
    // check for end of recursion
    if (count === 0) {
        points.push(a, b, c);
    } else {
        // bisect the sides
        var ab = mix(a, b, 0.5);
        var ac = mix(a, c, 0.5);
        var bc = mix(b, c, 0.5);

        // four new triangles (this is NOT a fractal!)
        count--;
        divideTriangle(a, ab, ac, count);
        divideTriangle(c, ac, bc, count);
        divideTriangle(b, bc, ab, count);
        divideTriangle(ab, ac, bc, count);
    }
}

// AngularJS UI logic
angular.module("tesselApp", [])
    .controller("TesselCtrl", [function () {
        var self = this;
        NumTimesToSubdivide = self.steps = 5;
        DegreesToRotate = self.rotation = 30;
        self.recalculate = function () {
            NumTimesToSubdivide = parseInt(self.steps);
            tesselateAndLoadTriangle();
            render();
        };
        self.redraw = function () {
            DegreesToRotate = parseInt(self.rotation);
            render();
        };
    }]);
