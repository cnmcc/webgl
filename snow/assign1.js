"use strict";

var canvas;
var gl;

var pointsA = [];

var pointsB = [];

var numTimesToSubdivide = 6;

var theta = Math.PI/4 ;
var thetaLoc;

function init()
{
    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    //
    //  Initialize our data for the Sierpinski Gasket
    //

    // First, initialize the corners of our gasket with three points.

	var vertices = [
	vec2( -0.86, -0.5 ),
            vec2(  0,  1 ),
            vec2(  0.86, -0.5 )
	];
    divideTriangle( vertices[0][0], vertices[0][1], vertices[1][0], vertices[1][1], vertices[2][0], vertices[2][1],
        numTimesToSubdivide, theta);

    //
    //  Configure WebGL
    //
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    //  Load shaders and initialize attribute buffers

    var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Load the data into the GPU

    var bufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, bufferId );
    gl.bufferData( gl.ARRAY_BUFFER, 50000, gl.STATIC_DRAW );
    gl.bufferSubData(gl.ARRAY_BUFFER, 0, flatten(pointsA));
	
	gl.bufferSubData(gl.ARRAY_BUFFER, 0, flatten(pointsB));

    // Associate out shader variables with our data buffer

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    thetaLoc = gl.getUniformLocation(program, "theta");

    document.getElementById("sliderr").onchange = function(event) {
        numTimesToSubdivide = parseInt(event.target.value);
		console.log("*************anumTimesToSubdivide:", numTimesToSubdivide);
		gl.uniform1f( gl.getUniformLocation(program, "numTimesToSubdivide"), numTimesToSubdivide);
        render();
    };

    document.getElementById("slidert").onchange = function(event) {
        theta = parseFloat(event.target.value);
		console.log("*************aatheta:", theta);
		gl.uniform1f( gl.getUniformLocation(program, "theta"), theta);
        render();
    };

    render();
}

function triangleA( ax, ay, bx, by, cx, cy)
{
    pointsA.push( vec2(ax,ay), vec2(bx,by), vec2(cx,cy) );
}

function divideTriangle( ax,ay, bx,by, cx,cy, count, angleRotate)
{

    // check for end of recursion

    if ( count === 0 ) {
        //triangleA( ax,ay, bx,by, cx,cy);
		twistTriangle( ax, ay, bx, by, cx, cy, angleRotate);
    }
    else {

        //bisect the sides

        var abx = ( ax + bx )/2;
        var aby = ( ay + by )/2;
        var acx = ( ax + cx )/2;
        var acy = ( ay + cy )/2;
        var bcx = ( bx + cx )/2;
        var bcy = ( by + cy )/2;

        --count;

        // three new triangles

        divideTriangle( ax, ay, abx, aby, acx, acy, count , angleRotate); //( a, ab, ac, count )
        divideTriangle( cx, cy, acx, acy, bcx, bcy, count , angleRotate); //( c, ac, bc, count )
        divideTriangle( bx, by, bcx, bcy, abx, aby, count, angleRotate ); //( b, bc, ab, count );
    }
}

function twistTriangle( ax, ay, bx, by, cx, cy, angleRotate)
{
    console.log("before", ax, ay);//, b, c);
    var aSqrt = Math.sqrt(Math.pow(ax,2)  + Math.pow(ay,2));
    var angle = angleRotate*aSqrt;
    console.log("angle", angle);

    var holder1 = ax;
    var holder2 = ay;
    ax = holder1*Math.cos(angle) - holder2*Math.sin(angle);
    ay = holder1*Math.sin(angle) + holder2*Math.cos(angle);

    var bSqrt = Math.sqrt(Math.pow(bx,2)  + Math.pow(by,2));
    angle = angleRotate*bSqrt;
    var holder3 = bx;
    var holder4 = by;
    bx = holder3*Math.cos(angle) - holder4*Math.sin(angle);
    by = holder3*Math.sin(angle) + holder4*Math.cos(angle);

    var cSqrt = Math.sqrt(Math.pow(cx,2)  + Math.pow(cy,2));
    angle = angleRotate*cSqrt;
    var holder5 = cx;
    var holder6 = cy;
    cx = holder5*Math.cos(angle) - holder6*Math.sin(angle);
    cy = holder5*Math.sin(angle) + holder6*Math.cos(angle);
    console.log("after", ax, ay, bx, by);
    console.log("after", cx, cy);
	//points = [];
    triangleA( ax,ay, bx,by, cx,cy );
}

window.onload = init;

function render()
{
    gl.clear( gl.COLOR_BUFFER_BIT );
    //gl.drawArrays( gl.TRIANGLES, 0, pointsA.length );
	//gl.drawArrays(gl.LINE_LOOP, 0, 3);
	
	var index;
	for (index = 0; index < pointsA.length; index += 3)
	{
		// Draw the triangle outline.
		gl.drawArrays(gl.LINE_LOOP, index, 3);
	}
    pointsA = [];
	//setTimeout(function(){requestAnimFrame(init);}, 10);
	
    requestAnimFrame(init);
}
