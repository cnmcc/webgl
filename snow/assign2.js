"use strict";
var canvas;
var gl;

var maxNumTriangles = 200;
var maxNumVertices  = 3 * maxNumTriangles;
var index = 0;

var redraw = false;
var very_big_size = 108000; // feel free to make this number as big as you need

var line_wid = 1.0;

var t;
var numLines = 0; //numLines
var numIndices = []; 
numIndices[0] = 0;
var start = [0]; 

var colors = [
vec4( 0.0, 0.0, 0.0, 1.0 ), // black
vec4( 1.0, 0.0, 0.0, 1.0 ), // red
vec4( 1.0, 1.0, 0.0, 1.0 ), // yellow
vec4( 0.0, 1.0, 0.0, 1.0 ), // green
vec4( 0.0, 0.0, 1.0, 1.0 ), // blue
vec4( 1.0, 0.0, 1.0, 1.0 ), // magenta
vec4( 0.0, 1.0, 1.0, 1.0) // cyan
]; 

var cindex = 1;

window.onload = function init() {
    canvas = document.getElementById( "gl-canvas" );
 gl = WebGLUtils.setupWebGL( canvas );
if ( !gl ) { alert( "WebGL isn't available" ); }

	var m = document.getElementById("mymenu");
	m.addEventListener("click", function() {
		cindex = m.selectedIndex;
	}); 

	//var c = document.getElementById("html5colorpicker").value;
	//console.log("-----color=",c);
	
    canvas.addEventListener("mousedown", function(event){
      redraw = true;
    });

    canvas.addEventListener("mouseup", function(event){
      redraw = false;
    });
    //canvas.addEventListener("mousedown", function(){
    canvas.addEventListener("mousemove", function(event){

        if(redraw) {
          gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
          var t = vec2(2*event.clientX/canvas.width-1,
           2*(canvas.height-event.clientY)/canvas.height-1);
        gl.bufferSubData(gl.ARRAY_BUFFER, 8*index, flatten(t));

        gl.bindBuffer(gl.ARRAY_BUFFER, cBuffer);
        t = vec4(colors[cindex]);
		
        gl.bufferSubData(gl.ARRAY_BUFFER, 16*index, flatten(t));
        index++;
      }

    } );

    var a = document.getElementById("Button1")
    a.addEventListener("click", function(){
        gl.viewport( 0, 0, canvas.width, canvas.height );
		gl.clearColor( 0.5, 0.5, 0.5, 1.0 );
		gl.clear( gl.COLOR_BUFFER_BIT );
		index = 0;
		render();
    });
	
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.5, 0.5, 0.5, 1.0 );
    //
    //  Load shaders and initialize attribute buffers
    //
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );


    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData( gl.ARRAY_BUFFER, 8*very_big_size, gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, 16*very_big_size, gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );

	//console.log("-----line-width=",line_wid);
    render();

}
/*
My strategy is as follows :
    mouseDown event, create a new buffer, currentpointsArray, store current color
    mouseMove event, add point to currentPoints array
    mouseUp event, store current buffer, points, color in an array, delete current buffer, reset currentpointsarray
    render, draw the current buffer, iterate through all previous buffers and draw
*/
function render() {
    gl.clear( gl.COLOR_BUFFER_BIT );
	gl.lineWidth(line_wid);
    gl.drawArrays( gl.POINTS, 0, index );

    window.requestAnimFrame(render);
	
	//gl.lineWidth(line_wid);
    //gl.clear( gl.COLOR_BUFFER_BIT );
    //gl.drawArrays( gl.POINTS, 0, index );
	//gl.drawArrays( gl.LINE_STRIP, 0, index );
	
	//for(var i=0; i<numLines; i++) {
	//	gl.drawArrays( gl.LINE_STRIP, start[i], numIndices[i] );
	//} 

    //window.requestAnimFrame(render);
}
