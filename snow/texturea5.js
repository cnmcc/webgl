"use strict";
var canvas;
var gl;
//textureCubev2
var numVertices  = 36;

var texSize = 256;//64 256
var flag = false;//off auto-rotate first scene

// Create a checkerboard pattern using floats  
var image1 = new Array()
    for (var i =0; i<texSize; i++)  image1[i] = new Array();
    for (var i =0; i<texSize; i++) 
        for ( var j = 0; j < texSize; j++) 
           image1[i][j] = new Float32Array(4);
    for (var i =0; i<texSize; i++) for (var j=0; j<texSize; j++) {
        var c = (((i & 0x8) == 0) ^ ((j & 0x8)  == 0));
        image1[i][j] = [c, c, c, 1];
    }
// Convert floats to ubytes for texture
var image2 = new Uint8Array(4*texSize*texSize);

    for ( var i = 0; i < texSize; i++ ) 
        for ( var j = 0; j < texSize; j++ ) 
           for(var k =0; k<4; k++) 
                image2[4*texSize*i+4*j+k] = 255*image1[i][j][k];
        
var pointsArray = [];
var colorsArray = [];
var texCoordsArray = [];

var texCoord = [
    vec2(0, 0),
    vec2(0, 1),
    vec2(1, 1),
    vec2(1, 0)
];
var vertices = [
    vec4( -0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.5,  0.5,  0.5, 1.0 ),
    vec4( 0.5,  0.5,  0.5, 1.0 ),
    vec4( 0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.5, -0.5, -0.5, 1.0 ),
    vec4( -0.5,  0.5, -0.5, 1.0 ),
    vec4( 0.5,  0.5, -0.5, 1.0 ),
    vec4( 0.5, -0.5, -0.5, 1.0 )
];
var vertexColors = [
    //vec4( 0.0, 0.0, 0.0, 1.0 ),  // black
	vec4( 1.0, 1.0, 1.0, 1.0 ), //white
    vec4( 1.0, 0.0, 0.0, 1.0 ),  // red
    vec4( 1.0, 1.0, 0.0, 1.0 ),  // yellow
    vec4( 0.0, 1.0, 0.0, 1.0 ),  // green
    vec4( 0.0, 0.0, 1.0, 1.0 ),  // blue
    vec4( 1.0, 0.0, 1.0, 1.0 ),  // magenta
    vec4( 0.0, 1.0, 1.0, 1.0 )   // cyan
];        
window.onload = init;


var xAxis = 0;
var yAxis = 1;
var zAxis = 2;
var axis = yAxis;

var theta = [0.0, 0.0, 0.0];

var thetaLoc;

function configureTexture(image) {
    //texture = gl.createTexture();
    gl.activeTexture( gl.TEXTURE0 );//TEXTURE2 with multi-texture
    gl.bindTexture( gl.TEXTURE_2D, gl.createTexture() );
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, texSize, texSize, 0, gl.RGBA, gl.UNSIGNED_BYTE, image);
    gl.generateMipmap( gl.TEXTURE_2D );
    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR ); //use diff attribute for sphere
    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST );
}
var texture;
function configureTexture1( image ) {
    texture = gl.createTexture();
    gl.bindTexture( gl.TEXTURE_2D, texture );
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D( gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image );
    gl.generateMipmap( gl.TEXTURE_2D );
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    //gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR );
    //gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST );
    gl.uniform1i(gl.getUniformLocation(program, "texture"), 0);
}

function quad(a, b, c, d) {

     pointsArray.push(vertices[a]); 
     colorsArray.push(vertexColors[a]); 
     texCoordsArray.push(texCoord[0]);

     pointsArray.push(vertices[b]); 
     colorsArray.push(vertexColors[a]);
     texCoordsArray.push(texCoord[1]); 

     pointsArray.push(vertices[c]); 
     colorsArray.push(vertexColors[a]);
     texCoordsArray.push(texCoord[2]); 
    
     pointsArray.push(vertices[a]); 
     colorsArray.push(vertexColors[a]);
     texCoordsArray.push(texCoord[0]); 

     pointsArray.push(vertices[c]); 
     colorsArray.push(vertexColors[a]);
     texCoordsArray.push(texCoord[2]); 

     pointsArray.push(vertices[d]); 
     colorsArray.push(vertexColors[a]);
     texCoordsArray.push(texCoord[3]); 
}
function colorCube()
{
    quad( 1, 0, 3, 2 );
    quad( 2, 3, 7, 6 );
    quad( 3, 0, 4, 7 );
    quad( 6, 5, 1, 2 );
    quad( 4, 5, 6, 7 );
    quad( 5, 4, 0, 1 );
}

var indexData = [];
var vertexPositionData = [];
var normalData = [];
var textureCoordData = [];

function setupSphere(){
        var latitudeBands = 30;
        var longitudeBands = 30;
        var radius = 0.8;  //adjust later
        
        for (var latNumber=0; latNumber <= latitudeBands; latNumber++) {
            var theta = latNumber * Math.PI / latitudeBands;
            var sinTheta = Math.sin(theta);
            var cosTheta = Math.cos(theta);

            for (var longNumber=0; longNumber <= longitudeBands; longNumber++) {
                var phi = longNumber * 2 * Math.PI / longitudeBands;
                var sinPhi = Math.sin(phi);
                var cosPhi = Math.cos(phi);

                var x = cosPhi * sinTheta;
                var y = cosTheta;
                var z = sinPhi * sinTheta;
                var u = 1 - (longNumber / longitudeBands);
                var v = 1 - (latNumber / latitudeBands);

                //normalData.push(x);
                //normalData.push(y);
                //normalData.push(z);
                textureCoordData.push(u);
                textureCoordData.push(v);
                vertexPositionData.push(radius * x);
                vertexPositionData.push(radius * y);
                vertexPositionData.push(radius * z);
				colorsArray.push(vertexColors[0]); //blue
            }
        }
        for (var latNumber=0; latNumber < latitudeBands; latNumber++) {
            for (var longNumber=0; longNumber < longitudeBands; longNumber++) {
                var first = (latNumber * (longitudeBands + 1)) + longNumber;
                var second = first + longitudeBands + 1;
                indexData.push(first);
                indexData.push(second);
                indexData.push(first + 1);

                indexData.push(second);
                indexData.push(second + 1);
                indexData.push(first + 1);
            }
        }
}
var program;
function init() {
    canvas = document.getElementById( "gl-canvas" );
    
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.0, 0.0, 0.0, 1.0 );
    
    gl.enable(gl.DEPTH_TEST);

    //
    //  Load shaders and initialize attribute buffers
    //
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );
    
    //colorCube();//cube

	setupSphere();
	
//console.log("-1-textureCoordData",textureCoordData);
//console.log("-2-vertexPositionData",vertexPositionData);
//console.log("-3-indexData",indexData);
//console.log("-4-colorsArray",colorsArray);

//COLOR
    var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer);
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colorsArray), gl.STATIC_DRAW );
    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer(vColor, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vColor);
	
//Vertice position
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer);
    //gl.bufferData( gl.ARRAY_BUFFER, flatten(vertexPositionData), gl.STATIC_DRAW); //pointsArray//old
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexPositionData), gl.STATIC_DRAW);
	vBuffer.itemSize = 3;
	vBuffer.numItems = vertexPositionData.length / 3;
	
    var vPosition = gl.getAttribLocation( program, "vPosition" );
    //gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0);old
	 gl.vertexAttribPointer(vPosition, vBuffer.itemSize, gl.FLOAT, false, 0, 0);//new
     gl.enableVertexAttribArray(vPosition);
    
//Texture coordinator	
    var tBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, tBuffer);
    //gl.bufferData( gl.ARRAY_BUFFER, flatten(textureCoordData), gl.STATIC_DRAW );//texCoordsArray
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoordData), gl.STATIC_DRAW);
	tBuffer.itemSize = 2;
	tBuffer.numItems =  textureCoordData.length / 2;
	
    var vTexCoord = gl.getAttribLocation( program, "vTexCoord");
    gl.vertexAttribPointer(vTexCoord, 2, gl.FLOAT, false, 0, 0);//old
	gl.vertexAttribPointer(vTexCoord, tBuffer.itemSize, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vTexCoord);
//Index
    var  indexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexData), gl.STREAM_DRAW);
    indexBuffer.itemSize = 1;
    indexBuffer.numItems = indexData.length;
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);

//https://s3.amazonaws.com/glowscript/textures/flower_texture.jpg
//http://postimg.org/image/w68hfjhhp/a51138a3/
//http://s22.postimg.org/7cyxevyhd/earth_map6.jpg

    mainImage = image2;
	configureTexture(mainImage);
     document.getElementById("Image Option").onclick = function( event) {
          //switch(event.srcElement.index) { //event.target.index
          switch(event.target.index) {
            case 0:
               mainImage = image2;
			   configureTexture(mainImage);
               break;
            case 1:
              	var image3 = document.getElementById("texImage");
				console.log("---image3",image3);
				//---image3 <img id="texImage" hidden="" src="earth_map6_jpg.jpg">
				//---image3 <img id="texImage" hidden="" alt="s3.amazonaws.com" src="https://s3.amazonaws.com/glowscript/textures/flower_texture.jpg">
				configureTexture1(image3);
               break;
			/*
			case 2:
               
			   //<br>
				//<input type="file" id="fileChooser" style="height:28px; width:175px;" hidden/>
			   var _URL = window.URL || window.webkitURL;
				$("#fileChooser").change(function(e) {
				var file;
				if ((file = this.files[0])) {
					imageFrURL = new Image();
        
					imageFrURL.onload = function() {
					alert("The image width is " +this.width + " and image height is " + this.height);
					};
					imageFrURL.src = _URL.createObjectURL(file);
				}
				});
				mainImage = imageFrURL;
				
				var image3 = document.getElementById("texImage");
				configureTexture1(image3);
               break;
				*/
        };
    };
       
	   

    thetaLoc = gl.getUniformLocation(program, "theta"); 
    document.getElementById("ButtonX").onclick = function(){
		axis = xAxis;
		theta[xAxis] += 2.0;
	};
    document.getElementById("ButtonY").onclick = function(){
		axis = yAxis;
		theta[yAxis] += 2.0;
	};
    document.getElementById("ButtonZ").onclick = function(){
		axis = zAxis;
		theta[zAxis] += 2.0;
	};
	document.getElementById("ButtonT").onclick = function(){flag = !flag;};                              
    render();
}

var imageFrURL;
var mainImage;

var render = function() {
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    //theta[axis] += 2.0;
	if(flag) theta[axis] += 2.0;
    gl.uniform3fv(thetaLoc, theta);
	gl.drawElements(gl.TRIANGLES, indexData.length, gl.UNSIGNED_SHORT, 0);
    requestAnimFrame(render);
}
